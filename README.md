# RISC-V para propósitos académicos

En este repositorio se encuentra una descripción en VHDL de un microprocesador compatible con la Arquitectura RISC-V. 

Aquí también encontrará un entorno de simulación con herramientas libres para HDL, el cual incluye una imagen de Docker con:

- GHDL
- Coco TestBench

# Uso de la imagen docker:

Clonar el repositorio usando `git`. Desde una terminal, ejecutar:

`git clone git@gitlab.com:unsl-up/risc-v.git`

Nota: Si no posee una clave SSH, sigua los pasos explicados [aquí](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair) para generarla y asociarla a su host.

Una vez clonado el repositorio, entre al directorio del mismo:

`cd risc-v`

## En Windows:

Para utilizar la imagen de docker, ejecute el siguiente comando:

`.\docker\dockershell_win.bat`

## En Linux:

Para utilizar la imagen de docker, ejecute el siguiente comando:

`docker/dockershell`


# Herramientas Extras

[Aquí](doc/Instalacion.md) puede encontrar los pasos de instalación de Docker y Git, tanto para Windows como para Linux.
